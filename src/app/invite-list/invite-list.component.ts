import {
  UserPostResponse,
  InviteService,
  User,
  UserPostStatus,
} from './../service/invite.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invite-list',
  templateUrl: './invite-list.component.html',
  styleUrls: ['./invite-list.component.css'],
})
export class InviteListComponent implements OnInit {
  users: User[];
  isLoading = true;
  successResponses: UserPostResponse[] = [];
  failedResponses: UserPostResponse[] = [];

  constructor(private inviteService: InviteService) {
    // get current invited users list
    this.inviteService.get().subscribe(
      (res) => this.users = res,
      () => {},
      () => (this.isLoading = false)
    );
    // get last invited users response if there's any
    this.inviteService.getLastInvitedUserResponses().forEach((item) => {
      if (item.status === UserPostStatus.Success) {
        // push success response into success state
        this.successResponses.push(item);
      } else {
        // push failed response into failed state
        this.failedResponses.push(item);
      }
    });
  }

  ngOnInit(): void {}
}
