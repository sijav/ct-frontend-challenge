import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InviteService, User } from '../service/invite.service';

const users: User[] = [
  { email: 'user0@comtravo.com' },
  { email: 'user1@comtravo.com' },
  { email: 'user2@comtravo.com' },
  { email: 'user3@comtravo.com' },
  { email: 'user4@comtravo.com' },
  { email: 'user5@comtravo.com' },
  { email: 'user6@comtravo.com' },
  { email: 'user7@comtravo.com' },
  { email: 'user8@comtravo.com' },
  { email: 'user9@comtravo.com' },
  { email: 'user10@comtravo.com' },
];

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.css'],
})
export class InviteComponent implements OnInit {
  constructor(private inviteService: InviteService, private router: Router) {}
  userlist: User[];
  isSending = false;

  ngOnInit(): void {
    console.log(users);
    this.userlist = [...users];
  }

  onSubmit(): void {
    // sending state true
    this.isSending = true;
    this.inviteService.inviteMany(this.userlist).subscribe(
      // we don't need to do anything per user successfuly invited
      () => {},
      // we don't need to do anything per user failure
      () => {},
      // after everything is done we can go over to home page for summary
      () => {
        this.isSending = false;
        this.router.navigate(['/']);
      }
    );
  }
}
