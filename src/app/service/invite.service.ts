import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface User {
  id?: number;
  email: string;
}

// user invite post status
export enum UserPostStatus {
  Success,
  Conflict,
  Failed,
}

// response of the user invite post
export interface UserPostResponse {
  id?: number;
  email: string;
  status: UserPostStatus;
  errorMessage?: string;
}

@Injectable({
  providedIn: 'root',
})
export class InviteService {
  private readonly url = 'http://localhost:3000/users';

  lastInvitedUsersResponse: UserPostResponse[] = [];

  constructor(private http: HttpClient) {}

  // get already invited users
  get(): Observable<User[]> {
    // get the user return observable
    return this.http.get<User[]>(this.url);
  }

  // simple invite
  invite(user: User): Observable<User> {
    // send the user and return observable
    return this.http.post<User>(this.url, user);
  }

  // get the lastet ongoing user responses
  getLastInvitedUserResponses(): UserPostResponse[] {
    // return the latest saved response
    return this.lastInvitedUsersResponse;
  }

  // invite many users
  inviteMany(users: User[]): Observable<UserPostResponse> {
    // empty the last response
    this.lastInvitedUsersResponse = [];
    // return an observable in which return on every user success and then complete it
    return new Observable<UserPostResponse>((sub) => {
      // for each user we need to post the user
      users.forEach((user) => {
        // invite the user
        this.invite(user).subscribe(
          (response) => {
            // if it is successful make the success result
            const result = {
              ...user,
              ...response,
              status: UserPostStatus.Success,
            };
            // push the result into saved responses
            this.lastInvitedUsersResponse.push(result);
            // return it to listening end user
            sub.next(result);
            // if this was the last response
            if (this.lastInvitedUsersResponse.length === users.length) {
              // finish the observable
              sub.complete();
            }
          },
          (error: HttpErrorResponse) => {
            // if it is successful make the fail result
            let result: UserPostResponse;
            switch (error.status) {
              // conflict problem
              case 409:
                result = {
                  ...user,
                  status: UserPostStatus.Conflict,
                  errorMessage: 'Email conflicts with another',
                };
                break;
              // unknown internal problem such as 500 error
              default:
                result = {
                  ...user,
                  status: UserPostStatus.Failed,
                  errorMessage: 'Unknown problem has occurred',
                };
                break;
            }
            // push the result into saved responses
            this.lastInvitedUsersResponse.push(result);
            // return it to listening end user
            sub.next(result);
            // if this was the last response
            if (this.lastInvitedUsersResponse.length === users.length) {
              // finish the observable
              sub.complete();
            }
          }
        );
      });
    });
  }
}
